
from json import dumps
from kafka import KafkaProducer
from time import sleep

#
# Simple Apache Kafka Producer
#
# (C) 2021 Tom Plese <tom@tomplese.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

if __name__=="__main__":

    producer = KafkaProducer(bootstrap_servers=['bigdatastack:9092'],
                             value_serializer=lambda x:dumps(x).encode('utf-8'))

    for i in range(1000):
        data = {'No' : i}
        producer.send('exampleTopic', value=data)
        sleep(0.001)